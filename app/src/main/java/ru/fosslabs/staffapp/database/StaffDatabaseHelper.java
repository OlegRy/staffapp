package ru.fosslabs.staffapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class StaffDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "staff.db";
    private static final int DATABASE_VERSION = 2;

    public StaffDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StaffTable.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        StaffTable.onUpgrade(db, oldVersion, newVersion);
    }
}
