package ru.fosslabs.staffapp.database;

import android.database.sqlite.SQLiteDatabase;

public class StaffTable {

    public static final String TABLE_STAFF = "staff";
    public static final String ID = "_id";
    public static final String PHOTO_PATH = "photo_path";
    public static final String NAME = "name";

    private static final String CREATE_TABLE_STAFF = "CREATE TABLE " + TABLE_STAFF + " (" +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PHOTO_PATH + " TEXT, " + NAME +
            " VARCHAR(100))";

    private static final String DROP_TABLE_STAFF = "DROP TABLE IF EXISTS " + TABLE_STAFF;

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_STAFF);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE_STAFF);
        onCreate(db);
    }

}
