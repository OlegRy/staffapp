package ru.fosslabs.staffapp.database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class StaffContentProvider extends ContentProvider {

    private StaffDatabaseHelper mDbHelper;

    private static final int STAFF = 10;
    private static final int EMPLOYEE_ID = 20;

    private static final String AUTHORITY = "ru.fosslabs.staffapp.contentprovider";
    private static final String BASE_PATH = "staff";

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/staff";
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "employee";

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(AUTHORITY, BASE_PATH, STAFF);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", EMPLOYEE_ID);
    }

    @Override
    public boolean onCreate() {
        mDbHelper = new StaffDatabaseHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        checkColumns(projection);
        queryBuilder.setTables(StaffTable.TABLE_STAFF);

        int uriType = sUriMatcher.match(uri);
        switch (uriType) {
            case STAFF:
                break;
            case EMPLOYEE_ID:
                queryBuilder.appendWhere(StaffTable.ID + " = " + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Incorrect uri: " + uri);
        }

        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Cursor c = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sUriMatcher.match(uri);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        long id = 0;

        switch (uriType) {
            case STAFF:
                id = db.insert(StaffTable.TABLE_STAFF, null, values);
                break;
            default:
                throw new IllegalArgumentException("Incorrect uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sUriMatcher.match(uri);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        int rowsDeleted = 0;

        switch (uriType) {
            case EMPLOYEE_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = db.delete(StaffTable.TABLE_STAFF, StaffTable.ID + " = " + id, null);

                } else {
                    rowsDeleted = db.delete(StaffTable.TABLE_STAFF, StaffTable.ID + " = " + id +
                    " and " + selection, selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Incorrect uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int uriType = sUriMatcher.match(uri);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        int rowsUpdated = 0;

        switch (uriType) {
            case STAFF:
                rowsUpdated = db.update(StaffTable.TABLE_STAFF, values, selection, selectionArgs);
                break;
            case EMPLOYEE_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = db.update(StaffTable.TABLE_STAFF,
                            values,
                            StaffTable.ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = db.update(StaffTable.TABLE_STAFF,
                            values,
                            StaffTable.ID + "=" + id
                                    + " and "
                                    + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Incorrect uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    private void checkColumns(String[] projection) {
        String[] available = { StaffTable.ID, StaffTable.PHOTO_PATH, StaffTable.NAME };
        if (projection != null) {
            Set<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            Set<String> availableColumns = new HashSet<String>(Arrays.asList(available));
            if (!availableColumns.containsAll(requestedColumns)) {
                throw new IllegalArgumentException("Unknow column in projection");
            }
        }
    }
}
