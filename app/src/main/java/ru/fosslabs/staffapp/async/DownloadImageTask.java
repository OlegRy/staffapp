package ru.fosslabs.staffapp.async;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.widget.ImageView;

import java.io.IOException;
import java.lang.ref.WeakReference;

public class DownloadImageTask extends AsyncTask<Uri, Void, Bitmap> {

    private WeakReference<ImageView> imageViewWeakReference;
    private WeakReference<Activity> activityWeakReference;

    public DownloadImageTask(ImageView iv_photo, Activity activity) {
        imageViewWeakReference = new WeakReference<ImageView>(iv_photo);
        activityWeakReference = new WeakReference<Activity>(activity);
    }

    @Override
    protected Bitmap doInBackground(Uri... params) {
        if (params.length != 0) {
            Activity activity = activityWeakReference.get();
            if (activity != null)
                try {
                    return MediaStore.Images.Media.getBitmap(activity.getContentResolver(), params[0]);
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if (bitmap != null) {
            ImageView iv_photo = imageViewWeakReference.get();
            if (iv_photo != null) {
                iv_photo.setImageBitmap(bitmap);
            }

        }
    }
}