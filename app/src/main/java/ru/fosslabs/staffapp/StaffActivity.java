package ru.fosslabs.staffapp;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import ru.fosslabs.staffapp.fragments.NewEmployeeDialogFragment;
import ru.fosslabs.staffapp.fragments.NewEmployeeFragment;
import ru.fosslabs.staffapp.fragments.StaffDetailFragment;
import ru.fosslabs.staffapp.fragments.StaffListFragment;


public class StaffActivity extends FragmentActivity implements StaffListFragment.Callback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff);

        if (findViewById(R.id.container) != null) {

            getSupportFragmentManager().beginTransaction().add(R.id.container, new StaffListFragment()).commit();
        } else {
            getSupportFragmentManager().beginTransaction().add(R.id.list_fragment, new StaffListFragment()).commit();
        }
    }

    @Override
    public void onListItemClick(int position, Uri uri) {

        StaffDetailFragment detailFragment = (StaffDetailFragment) getSupportFragmentManager()
                .findFragmentById(R.id.detail_fragment);
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
        if (detailFragment != null) {
            detailFragment.updateUI(uri);
        } else if (f == null) {
            detailFragment = new StaffDetailFragment();
            Bundle args = new Bundle();
            args.putParcelable(StaffDetailFragment.PARCELABLE, uri);
            args.putInt(StaffDetailFragment.POSITION, position);

            detailFragment.setArguments(args);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.detail_fragment, detailFragment);
            transaction.commit();
        } else {
            f = getSupportFragmentManager().findFragmentById(R.id.container);
            if (f instanceof StaffDetailFragment) {
                detailFragment = (StaffDetailFragment) f;
            }

            if (detailFragment != null) {
                detailFragment.updateUI(uri);
            } else {
                detailFragment = new StaffDetailFragment();
                Bundle args = new Bundle();
                args.putParcelable(StaffDetailFragment.PARCELABLE, uri);
                args.putInt(StaffDetailFragment.POSITION, position);

                detailFragment.setArguments(args);

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, detailFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == NewEmployeeFragment.FILE_PATH) {
                NewEmployeeFragment newEmployeeFragment = (NewEmployeeFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.container);
                if (newEmployeeFragment != null) {
                    newEmployeeFragment.updateUI(data.getData());
                } else {
                    NewEmployeeDialogFragment dialogFragment = new NewEmployeeDialogFragment();
                    Bundle args = new Bundle();
                    args.putParcelable(NewEmployeeDialogFragment.FILE_PATH, data.getData());
                    dialogFragment.setArguments(args);
                }
            }
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
}
