package ru.fosslabs.staffapp.adapters;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import ru.fosslabs.staffapp.R;
import ru.fosslabs.staffapp.async.DownloadImageTask;
import ru.fosslabs.staffapp.database.StaffContentProvider;
import ru.fosslabs.staffapp.database.StaffTable;
import ru.fosslabs.staffapp.fragments.NewEmployeeDialogFragment;
import ru.fosslabs.staffapp.fragments.NewEmployeeFragment;

public class EmployeeAdapter extends SimpleCursorAdapter {

    private FragmentActivity mContext;
    private LayoutInflater mInflater;

    private static class ViewHolder {
        private ImageView iv_photo;
        private TextView tv_name;
        private Button btn_settings;
    }

    public EmployeeAdapter(FragmentActivity context, String[] from, int[] to) {
        super(context, R.layout.employee_item, null, from, to, 0);
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return mInflater.inflate(R.layout.employee_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, final Cursor cursor) {
        Uri photoPath = Uri.parse(cursor.getString(cursor.getColumnIndex(StaffTable.PHOTO_PATH)));
        String name = cursor.getString(cursor.getColumnIndex(StaffTable.NAME));

        ViewHolder vh = new ViewHolder();
        vh.iv_photo = (ImageView) view.findViewById(R.id.iv_photo);
        vh.tv_name = (TextView) view.findViewById(R.id.tv_name);
        vh.btn_settings = (Button) view.findViewById(R.id.btn_settings);

        if (vh.iv_photo != null) {
            new DownloadImageTask(vh.iv_photo, mContext).execute(photoPath);
        }
        if (vh.tv_name != null) {
            vh.tv_name.setText(name);
        }
        if (vh.btn_settings != null) {
            vh.btn_settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mContext.getSupportFragmentManager().findFragmentById(R.id.container) != null) {
                        long id = cursor.getLong(cursor.getColumnIndex(StaffTable.ID));
                        Uri uri = Uri.parse(StaffContentProvider.CONTENT_URI + "/" + id);
                        NewEmployeeFragment newEmployeeFragment = new NewEmployeeFragment();
                        Bundle args = new Bundle();
                        args.putParcelable(NewEmployeeFragment.URI, uri);
                        newEmployeeFragment.setArguments(args);
                        FragmentTransaction transaction = mContext.getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.container, newEmployeeFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    } else {
                        long id = cursor.getLong(cursor.getColumnIndex(StaffTable.ID));
                        Uri uri = Uri.parse(StaffContentProvider.CONTENT_URI + "/" + id);
                        NewEmployeeDialogFragment newEmployeeDialogFragment= new NewEmployeeDialogFragment();
                        Bundle args = new Bundle();
                        args.putParcelable(NewEmployeeFragment.URI, uri);
                        newEmployeeDialogFragment.setArguments(args);
                        newEmployeeDialogFragment.show(mContext.getSupportFragmentManager(), "dialog");
                    }
                }
            });
        }
    }
}
