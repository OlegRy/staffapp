package ru.fosslabs.staffapp.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import ru.fosslabs.staffapp.R;
import ru.fosslabs.staffapp.database.StaffContentProvider;
import ru.fosslabs.staffapp.database.StaffTable;

public class NewEmployeeDialogFragment extends DialogFragment {

    public static final String FILE_PATH = "file_path";

    private Uri uri;
    private EditText et_name;
    private TextView tv_path;
    private Button btn_view;

    private Uri filePath;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_new_employee, null);

        uri = (savedInstanceState != null) ? (Uri) savedInstanceState.getParcelable(NewEmployeeFragment.URI) : null;

        if (args != null) {
            uri = args.getParcelable(NewEmployeeFragment.URI);
            filePath = args.getParcelable(FILE_PATH);
        }

        et_name = (EditText) view.findViewById(R.id.et_name);
        tv_path = (TextView) view.findViewById(R.id.tv_path);

        btn_view = (Button) view.findViewById(R.id.btn_view);

        btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(i, "choose image"),
                        NewEmployeeFragment.FILE_PATH);
            }
        });
        if (filePath != null) {
            tv_path.setText(filePath.toString());
        }
        if (uri != null) {
            fillData(uri);
        }
        builder.setTitle(R.string.new_employee);
        builder.setView(view);
        builder.setPositiveButton(R.string.new_employee_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveState();

                dialog.dismiss();
            }
        });
        builder.setNegativeButton(R.string.new_employee_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(NewEmployeeFragment.URI, uri);
    }

    private void saveState() {
        String photoPath = tv_path.getText().toString();
        String name = et_name.getText().toString();

        if (photoPath == null && name == null) {
            return;
        }

        ContentValues cv = new ContentValues();
        cv.put(StaffTable.PHOTO_PATH, photoPath);
        cv.put(StaffTable.NAME, name);

        if (uri == null) {
            uri = getActivity().getContentResolver().insert(StaffContentProvider.CONTENT_URI, cv);
        } else {
            getActivity().getContentResolver().update(uri, cv, null, null);
        }
    }

    private void fillData(Uri uri) {
        String[] projection = { StaffTable.ID, StaffTable.PHOTO_PATH, StaffTable.NAME };
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
            String photoPath = cursor.getString(cursor.getColumnIndex(StaffTable.PHOTO_PATH));
            String name = cursor.getString(cursor.getColumnIndex(StaffTable.NAME));

            tv_path.setText(photoPath);
            et_name.setText(name);
            cursor.close();
        }
    }
}
