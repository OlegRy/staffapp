package ru.fosslabs.staffapp.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import ru.fosslabs.staffapp.R;
import ru.fosslabs.staffapp.adapters.EmployeeAdapter;
import ru.fosslabs.staffapp.database.StaffContentProvider;
import ru.fosslabs.staffapp.database.StaffTable;

;

public class StaffListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private ListView lv_staff;
    private EmployeeAdapter mAdapter;

    public interface Callback {

        public void onListItemClick(int position, Uri uri);

    }

    private Callback mCallback;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_staff_list, container, false);
        setHasOptionsMenu(true);
        lv_staff = (ListView) v.findViewById(R.id.lv_staff);

        fillData();

        lv_staff.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Uri uri = Uri.parse(StaffContentProvider.CONTENT_URI + "/" + id);
                mCallback.onListItemClick(position, uri);
                lv_staff.setItemChecked(position, true);
            }
        });

        lv_staff.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, final long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(getResources().getString(R.string.delete));
                builder.setMessage(getResources().getString(R.string.delete_question));
                builder.setPositiveButton(getResources().getString(R.string.positive), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Uri uri = Uri.parse(StaffContentProvider.CONTENT_URI + "/" + id);
                        getActivity().getContentResolver().delete(uri, null, null);
                        fillData();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton(R.string.negative, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
                return true;
            }
        });
        return v;
    }

    private void fillData() {
        String[] from = { StaffTable.PHOTO_PATH, StaffTable.NAME };
        int[] to = { R.id.iv_photo, R.id.tv_name };
        getLoaderManager().initLoader(0, null, this);
        mAdapter = new EmployeeAdapter(getActivity(), from, to);

        lv_staff.setAdapter(mAdapter);
    }


    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = { StaffTable.ID, StaffTable.PHOTO_PATH, StaffTable.NAME };
        CursorLoader cursorLoader = new CursorLoader(getActivity(), StaffContentProvider.CONTENT_URI,
                projection, null, null, null);

        return cursorLoader;
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> cursorLoader, Cursor cursor) {
        mAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> cursorLoader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallback = (Callback) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.staff, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.me_add_employee:
                if (getActivity().getSupportFragmentManager().findFragmentById(R.id.container) != null) {
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, new NewEmployeeFragment());
                    transaction.addToBackStack(null);
                    transaction.commit();
                } else {
                    NewEmployeeDialogFragment dialogFragment = new NewEmployeeDialogFragment();
                    dialogFragment.show(getActivity().getSupportFragmentManager(), "dialog");
                }
                break;
        }
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getActivity().getSupportFragmentManager().findFragmentById(R.id.detail_fragment) != null) {
            lv_staff.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }
}
