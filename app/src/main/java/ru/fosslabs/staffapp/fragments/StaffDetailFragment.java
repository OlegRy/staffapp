package ru.fosslabs.staffapp.fragments;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ru.fosslabs.staffapp.R;
import ru.fosslabs.staffapp.async.DownloadImageTask;
import ru.fosslabs.staffapp.database.StaffTable;

public class StaffDetailFragment extends Fragment {

    private ImageView iv_photo;
    private TextView tv_name;

    public static final String PARCELABLE = "ru.fosslabs.staffapp.StaffDetailFragment.PARCELABLE";
    public static final String POSITION = "ru.fosslabs.staffapp.StaffDetailFragment.POSITION";

    private Uri uri;
    private int position;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            uri = savedInstanceState.getParcelable(PARCELABLE);
            position = savedInstanceState.getInt(POSITION);
        }

        Bundle args = getArguments();
        if (args != null) {
            uri = args.getParcelable(PARCELABLE);
            position = args.getInt(POSITION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_staff_detail, container, false);

        iv_photo = (ImageView) view.findViewById(R.id.iv_photo);
        tv_name = (TextView) view.findViewById(R.id.tv_name);

        fillData(uri);
        return view;
    }

    private void fillData(Uri uri) {
        String[] projection = { StaffTable.ID, StaffTable.PHOTO_PATH, StaffTable.NAME };

        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
            Uri photoPath = Uri.parse(cursor.getString(cursor.getColumnIndex(StaffTable.PHOTO_PATH)));
            String name = cursor.getString(cursor.getColumnIndex(StaffTable.NAME));

            new DownloadImageTask(iv_photo, getActivity()).execute(photoPath);
            tv_name.setText(name);
            cursor.close();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(PARCELABLE, uri);
        outState.putInt(POSITION, position);
    }

    public void updateUI(Uri uri) {
        fillData(uri);
    }
}
