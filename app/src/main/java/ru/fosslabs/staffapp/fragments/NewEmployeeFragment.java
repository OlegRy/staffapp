package ru.fosslabs.staffapp.fragments;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import ru.fosslabs.staffapp.R;
import ru.fosslabs.staffapp.database.StaffContentProvider;
import ru.fosslabs.staffapp.database.StaffTable;

public class NewEmployeeFragment extends Fragment {

    public static final String URI = "ru.fosslabs.staff.NewEmployeeFragment.URI";
    public static final int FILE_PATH = 0;

    private EditText et_name;
    private TextView tv_path;
    private Button btn_view;
    private Uri uri;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();

        uri = (savedInstanceState != null) ? (Uri) savedInstanceState.getParcelable(URI) : null;

        if (args != null) {
            uri = args.getParcelable(URI);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(URI, uri);
    }

    private void fillData(Uri uri) {
        String[] projection = { StaffTable.ID, StaffTable.PHOTO_PATH, StaffTable.NAME };
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
            String photoPath = cursor.getString(cursor.getColumnIndex(StaffTable.PHOTO_PATH));
            String name = cursor.getString(cursor.getColumnIndex(StaffTable.NAME));

            tv_path.setText(photoPath);
            et_name.setText(name);
            cursor.close();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new_employee, container, false);

        setHasOptionsMenu(true);

        et_name = (EditText) view.findViewById(R.id.et_name);
        tv_path = (TextView) view.findViewById(R.id.tv_path);
        btn_view = (Button) view.findViewById(R.id.btn_view);

        btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(i, "choose image"), FILE_PATH);
            }
        });
        if (uri != null) {
            fillData(uri);
        }
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.employee, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.me_ready:
                if (tv_path.getText().toString() != null && et_name.getText().toString() != null
                        && !tv_path.getText().toString().equals("") && !et_name.getText().toString().equals("")) {
                    saveState();
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, new StaffListFragment());
                    transaction.addToBackStack(null);
                    transaction.commit();
                    return true;
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.error);
                    builder.setMessage(R.string.error_message);
                    builder.setCancelable(false);
                    builder.setNegativeButton(R.string.error_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }

        }
        return false;
    }

    private void saveState() {
        String photoPath = tv_path.getText().toString();
        String name = et_name.getText().toString();

        if (photoPath == null && name == null) {
            return;
        }

        ContentValues cv = new ContentValues();
        cv.put(StaffTable.PHOTO_PATH, photoPath);
        cv.put(StaffTable.NAME, name);

        if (uri == null) {
            uri = getActivity().getContentResolver().insert(StaffContentProvider.CONTENT_URI, cv);
        } else {
            getActivity().getContentResolver().update(uri, cv, null, null);
        }
    }

    public void updateUI(Uri path) {
        tv_path.setText(path.toString());
    }
}
